
%dw 2.0
output application/java 
---
[{
    ID : 71,
    code1 : "commodo",
    code2 : "tempor",
    airlineName : "id",
    toAirport : "veniam,",
    fromAirport : "ex",
    takeOffDate : |2009-08-23|,
    price : 47.59,
    planeType : "vel",
    seatsAvailable : 99,
    totalSeats : 37,
  },
{
    ID : 98,
    code1 : "irure",
    code2 : "magnam",
    airlineName : "fugiat",
    toAirport : "ad",
    fromAirport : "eu",
    takeOffDate : |2000-09-07|,
    price : 36.42,
    planeType : "Ut",
    seatsAvailable : 82,
    totalSeats : 56,
  }]
